# Revision A

## Description
tba

## Implementation
Using a breadboard, solder connections to provide 5V supply from an MCU to the DHT11 and the Photoresistor. Solder connections to provide a 3.3V supply to the BMP180. Create a common ground connection for all three components. Solder a connection from the DHT11 to a digital port on the MCU. Repeat for the photoresistor. Solder connections from the SCL and SDA pins on the BMP180 to the appropriate ports on the MCU. Attach the breadboard to a base board or case, using stand-offs if necessary to prevent short circuits. 

## Connections to MCU

* +5v - powers DHT11 and the photoresistor
* +3.3v - to power the BMP180 breakout board
* gnd - common ground for all components
* signal wire from DHT11 to digital pin
* signal wire from photoresistor to digital pin
* BMP180
  * scl wire connects to scl port
  * sda wire connects to sda port
